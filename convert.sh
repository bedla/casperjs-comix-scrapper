#!/usr/bin/sh

#For security reasons replace / in find command with comix data directory 
for i in $(find / -name ArchiveeeRandomString0ok123qwe); do
    echo "$i";
    dir=$(dirname ${i});
    cd ${dir};
    F=$(file $i -b -i);
    if [[ $F == *rar* ]]
      then
      echo "It's RAR!";
      unrar e ./ArchiveeeRandomString0ok123qwe ./ArchiveData/;
    fi
    
    if [[ $F == *zip* ]]
      then
      echo "It's ZIP!";
      unzip ArchiveeeRandomString0ok123qwe -d ./ArchiveData/;
    fi
    
    zip -rj Archive.zip ArchiveData/*
    rm -rf ArchiveData/
    rm -f ArchiveeeRandomString0ok123qwe
    
done