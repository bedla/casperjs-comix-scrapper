/*jshint strict:false*/
/*global CasperError, console, phantom, require*/

/**
 *CONFIG START
**/
var user = "example@example.com";
var auth = "PA$$WORD";
var file_root = "/root/casper/orfeuss/data/"; //SLASH ON THE END IS REQUIRED
var script_root = "/root/casper/orfeuss/";    //SLASH ON THE END IS REQUIRED
/**
 *CONFIG END
**/


//DO NOT CHANGE THIS. min  and max are fallback variables, will be owerwriten in script
var min=0;
var max=100000;
var current_path = "";
var casper = require("casper").create({
    clientScripts: [script_root+"includes/jquery-1.11.2.min.js"],
    pageSettings: {
      webSecurityEnabled: false,
    },
    onWaitTimeout: function() {
        this.echo('** Wait-TimeOut **');
    },
    onStepTimeout: function() {
        this.echo('** Step-TimeOut **');
    }
});
var fs = require('fs');
var execFile = require("child_process").execFile

casper.start("http://comicbookplus.com/forum/?action=login&cbpqs=cbphome",
  function() {
      fs.changeWorkingDirectory(script_root+"tmp/");

      fs.write("scrapper.maxId",this.page.evaluate(getMaxId),"w");            

      this.echo(this.getTitle());

      console.log('Starting location is ' + this.getCurrentUrl());
  });

casper.then(function() {
      this.fillSelectors('form#frmLogin', {
          'input[name="user"]':    user,
          'input[name="passwrd"]': auth
      }, true);
});

casper.then(function() {
    console.log('Authentication ok, new location is ' + this.getCurrentUrl()); 
});

casper.then(function() {
  fs.changeWorkingDirectory(script_root+"tmp/");
  min = Number(fs.read("scrapper.lastId"));
  max = Number(fs.read("scrapper.maxId"));
  casper.echo("Scrapping comics with id from "+min+" to "+max+", PLEASE BE PATIENT");
  var dlid=min; 
  casper.repeat(max-min, function(){
  
      this.thenOpen(("http://comicbookplus.com/?dlid=" + dlid), function() {
          
           this.echo("PARSING http://comicbookplus.com/?dlid=" + dlid);
           if(/We Could Not Find It/.test(this.getTitle())){
             this.echo("404");
           }else{
              this.echo("Page title "+this.getTitle());
              
              var info;
              var genre;
                info = this.page.evaluate(getDescription);
                this.echo("Description: "+info);

                genre = this.page.evaluate(getGenre);

                if(genre==null){
                  genre = "Unknown";
                }
                this.echo("Genre: "+genre);
                poster = this.page.evaluate(getPosterSrc);                                          
                downloadUrl = this.page.evaluate(getDownloadUrl);

              makeDir(genre,this.getTitle().replace(/[^\x00-\x7F]/g, " ").replace(" - Comic Book Plus","").replace("#"," ").replace("*"," "));
              this.echo("Dowloading poster - " + poster);
              this.download(poster,"small.jpg");
              fs.write("desc.txt",info,"w");

                this.echo("Downloading comix http://comicbookplus.com"+downloadUrl);
                this.options.pageSettings['resourceTimeout']=240000;
                this.download("http://comicbookplus.com"+downloadUrl,"ArchiveeeRandomString0ok123qwe");
                this.options.pageSettings['resourceTimeout']=30000;
  
              this.then(function (){
  
                this.echo("SLEEPING 70s");
                this.wait(70000);
              });
           }
           
      })
      dlid++;
      changeLastId();
  })
});
casper.run();




/*
FUNCTIONS
*/
var getDescription =  function(){
      var r;
                  $("td").each(function(){
                                if($(this).text()=="Publication"){
                                  r = $(this).next("td").text();
                                 }
                  });
                  if(r==null){
                  $("td").each(function(){
                                if($(this).text()=="Publisher"){
                                  r = $(this).next("td").text();
                                 }
                  }                 
                );
                }
        
                return r.replace(/<(?:.|\n)*?>/gm, '').replace(/(\r\n|\n|\r)/gm,"").replace(/ +(?= )/g,'').replace(/[^\x00-\x7F]/g, "").replace(/ Price*?\|/,""); 
}

var getDownloadUrl =  function(){
      var r;
                  $("a").each(function(){
                                if($(this).text()=="Download File"){
                                  r = $(this).attr('href');
                                  return false;
                                 }
                                 return true;
                  });
        
                return r; 
}

var getGenre =  function(){
      var r;
      var resultString = "Unknown";
                  $("td").each(function(){
                                if($(this).text()=="Content"){
                                  r = $(this).next("td").text();
                                  return false;
                                 }
                                 return true;
                  });
                var split = r.split("|");
                $.each(split,function(i,val){
                     if(val.indexOf("Genre")>-1){
                        resultString = val.split(":")[1].trim();
                        return false;
                     }
                     return true;
                })
                if(resultString==null)return "Unknown";          
                return resultString.replace(/[^\x00-\x7F]/g, ""); 
}

var getPosterSrc =  function(){
      var r;
                  $("img").each(function(){
                                if(/thumb/.test($(this).attr('src'))){
                                  r = $(this).attr('src');
                                  return false;
                                 }
                                 return true;
                  });
                return r; 
}

function makeDir(genre, title){
            try{
            fs.changeWorkingDirectory(file_root);
            fs.makeDirectory(genre);
            fs.changeWorkingDirectory(file_root+genre);
            fs.makeDirectory(title);
            fs.changeWorkingDirectory(file_root+genre+"/"+title);
            }
            catch(err)
            {console.log("error:"+ err);};
  }
  

function changeLastId(){
      fs.changeWorkingDirectory(script_root+"tmp/");
      min = fs.read("scrapper.lastId");
      min++;
      fs.write("scrapper.lastId",min,"w");
} 

var getMaxId = function (){
  return $("#Wscrollframe table tr:first a:first").attr("href").replace("/?dlid=","").trim();
}
