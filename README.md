PREREQUISITES
-------------
### 1) install packages rar, unrar

On Fedora based
    
    wget http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.2-2.el6.rf.i686.rpm
    rpm -Uvh rpmforge-release-0.5.2-2.el6.rf.i686.rpm
    yum install rar unrar
    yum install zip unzip 

On Debian based:

    apt-get install zip unzip
    software-properties-gtk
    apt-get update
    apt-get install rar unrar

### 2) install phantomjs
   
    Fedora based:
      yum -y install gcc gcc-c++ make flex bison gperf ruby \
      openssl-devel freetype-devel fontconfig-devel libicu-devel sqlite-devel \
      libpng-devel libjpeg-devel
    
    Debian based:
      sudo apt-get install g++ flex bison gperf ruby perl \
      libsqlite3-dev libfontconfig1-dev libicu-dev libfreetype6 libssl-dev \
      libpng-dev libjpeg-dev
  
  Build from source, this will take really long time. This step require 2GB or more of free ram. If you have less, go to section "Build from source failed?"    
    
    git clone git://github.com/ariya/phantomjs.git
    cd phantomjs
    git checkout 2.0
    ./build.sh
    
   Build from source failed? try this.
   
    Debian based:
      https://gist.github.com/julionc/7476620
    Fedora based:
      http://www.sameerhalai.com/blog/how-to-install-phantomjs-on-a-centos-server/    

### 3) install casperjs

    git clone git://github.com/n1k0/casperjs.git
    cd casperjs
    ln -sf `pwd`/bin/casperjs /usr/local/bin/casperjs

### 4)  clone script.js and related folders, files from git.
    
    git clone git clone git@bitbucket.org:bedlaj/casperjs-comix-scrapper.git
    
./tmp , ./tmp/scrapper.lastId , ./tmp/scrapper.maxId MUST BE WRITABLE 

CONFIGURATION
--------------
In script.js header change variables user, auth, file_root, script_root
Folder specified in "file_root" must be writable

USAGE
-----
###scrapping script
  
    casperjs --web-security=no script.js
        
        
`THERE IS NO PROTECTION FOR RUNNING IN MULTIPLE INSTANCES.
PLEASE BE SURE, YOU ARE RUNNING ONLY ONE INSTANCE AT ONE TIME. 
AT LEAST INITIAL SCRAPPING RUN MANUALLY.
IF YOU RUN MULTIPLE INSTANCES, YOU WILL EXCEED SITE PROTECTION LIMIT AND YOUR ARCHIVES WILL BE CORRUPTED!`  

###Script for converting cbz/cbr to zip
    ./convert.sh
Ideally run from CRON. Recomended interval 1 hour.